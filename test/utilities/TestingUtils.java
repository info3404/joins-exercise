package utilities;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import access.read.AccessIterator;
import access.write.AccessInserter;
import buffer.BufferManager.BufferAccessException;
import disk.DataPage;
import heap.HeapFile;
import heap.Tuple;

/**
 * Helper utilities for an AccessIterator and HeapFile to assist with testing
 */
public class TestingUtils {

    public static int count(AccessIterator rows) {
        int i = 0;
        while(rows.hasNext()) {
            Tuple row = rows.next();
//            System.out.println("join " + i + ":" + row.toString());
            i++;
        }
        return i;
    }

    public static int count(HeapFile table) throws BufferAccessException {
        try(AccessIterator iterator = table.iterator()) {
            return count(iterator);
        }
    }

    public static int countPages(HeapFile table) throws BufferAccessException {
        try(AccessIterator iterator = table.iterator()) {
            int tuplesOnPage = DataPage.getMaxRecordsOnPage(iterator.getSchema());
            int i = 0;
            while(iterator.hasNext()) {
                if(iterator.next() != null) {
                    i += 1;
                };
            }
            return (int)Math.ceil(i / (double)tuplesOnPage);
        }
    }

    public static void insertRows(HeapFile table, Object[][] rows) throws BufferAccessException {
        try(AccessInserter inserter = table.inserter()) {
            for(Object[] row : rows) {
                inserter.insert(row);
            }
        }
    }

	/**
	 * @param results
	 * @param message
	 */
	public static void checkGetRecord(AccessIterator results, Object ... values) {
		assertTrue("No more tuples when expecting " + Arrays.toString(values), results.hasNext());
		final Tuple next = results.next();
		assertTrue("Wrong tuple - expected [" + Arrays.toString(values) + "], got " + next.toString() , next.rowEquals(values));
	}


}
