package join;

import access.read.AccessIterator;
import global.DatabaseManager;
import heap.HeapFile;
import heap.TupleDesc;
import join.data.SortedTestData;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import parser.JoinArgs;
import utilities.TestingUtils;

import static org.junit.Assert.*;
import static utilities.TestingUtils.checkGetRecord;

public class SortMergeJoinTest {

	// Comment this out for debugging
    @Rule
    public Timeout globalTimeout = new Timeout(1000);
    
	/**
	 * Depending upon the exact implementation, there may be some extra overhead
	 * in additional page accesses beyond the number calculated from the 
	 * algorithm. This factor gives some latitude on the actual cost.
	 */
	private static final double COST_TOLERANCE_FACTOR = 0.50;
	
    private HeapFile students;
    private HeapFile tutors;
    private DatabaseManager dbms;

    @Before
    public void setUp() throws Exception {
        dbms  = new DatabaseManager();
        
        // Create Test Schema for students
        TupleDesc studentSchema = new TupleDesc();
        studentSchema.addString("name").addInteger("age").addString("class").addBoolean("male");
        dbms.getCatalog().addSchema(studentSchema, "students");
        students = dbms.getHeapFile("students");
        
        // Create Test Schema for tutors
        TupleDesc tutorSchema = new TupleDesc();
        tutorSchema.addString("id").addString("tutor");
        dbms.getCatalog().addSchema(tutorSchema, "tutors");
        tutors = dbms.getHeapFile("tutors"); 
    }

    @After
    public void tearDown() throws Exception {
    	tutors = null;
    	students = null;
    	dbms.close();
    }

    /**
     * Simple example for case where each relation is contained in a single page
     */
    @Test
    public void testJoinSimple() throws Exception {
        TestingUtils.insertRows(students, SortedTestData.STUDENT_ROWS_SMALL_SORTED);
        TestingUtils.insertRows(tutors, SortedTestData.TUTOR_ROWS_SMALL_SORTED);
        // We will join the two relations based upon class==id
        // NOTE: First argument (inner relation name) used by query processor, not used here 
        JoinArgs args = new JoinArgs("tutors", "class", "id");

        try(AccessIterator results = new SortMergeJoin(students.iterator(), tutors.iterator(), args)) {
            checkGetRecord(results, "Roger", 20, "INFO1103", true, "INFO1103", "Joshua");
            checkGetRecord(results, "Roger", 20, "INFO1103", true, "INFO1103", "Scott");
            checkGetRecord(results, "Michael", 19, "INFO1103", true, "INFO1103", "Joshua");
            checkGetRecord(results, "Michael", 19, "INFO1103", true, "INFO1103", "Scott");
            checkGetRecord(results, "Jan", 18, "INFO1903", false, "INFO1903", "Steven");
            checkGetRecord(results, "John", 18, "INFO1903", false, "INFO1903", "Steven");
            assertFalse("Returning too many results", results.hasNext());
        }
    }
    
    /**
     * Larger example in which both relations span multiple pages
     */
	@Test
    public void testJoinLarge() throws Exception {
        TestingUtils.insertRows(students, SortedTestData.STUDENT_ROWS_SORTED);
        TestingUtils.insertRows(tutors, SortedTestData.TUTOR_ROWS_SORTED);
        JoinArgs args = new JoinArgs("tutors", "class", "id");
//        final int expectedCost = 16 + 6;
        final int nRowsExpected = 8089;
        
        try(AccessIterator r=students.iterator(); AccessIterator s = tutors.iterator()) {
	        final int na = getAccessCount();
	        try(AccessIterator results = new SortMergeJoin(r, s, args)) {
	        	final int nb = getAccessCount();
	        	assertEquals("Join constructor should not do most of the work", 1.5, nb-na, 1.5);
	            assertEquals("The number of tuples returned isn't right", nRowsExpected, TestingUtils.count(results));
	            // Cost is hard to estimate in the general case of non-unique values
	            //				assertEquals("Number of pages should be similar to expected cost of join ", expectedCost, getAccessCount() - nb, expectedCost*COST_TOLERANCE_FACTOR);
	        }
        }
    }
    
    /**
     * Same as testJoinLarge but for simpler case of unique values for join attributes
     */
	@Test
    public void testJoinLargeUnique() throws Exception {
        TestingUtils.insertRows(students, SortedTestData.STUDENT_ROWS_SORTED_UNIQUE);
        TestingUtils.insertRows(tutors, SortedTestData.TUTOR_ROWS_SORTED_UNIQUE);
        JoinArgs args = new JoinArgs("tutors", "class", "id");
        // Last match occurs for COMP1327, on page 5 of students and page 6 of tutors 
        final int expectedCost = 5 + 6; // Worst case would be 16 + 6;
        final int nRowsExpected = 55;

        try(AccessIterator r=students.iterator(); AccessIterator s = tutors.iterator()) {
            final int na = getAccessCount();
	        try(AccessIterator results = new SortMergeJoin(r, s, args)) {
	        	final int nb = getAccessCount();
	        	assertEquals("Join constructor should not do most of the work", 1.5, nb-na, 1.5);
	            assertEquals("The number of tuples returned isn't right", nRowsExpected, TestingUtils.count(results));
				assertEquals("Number of pages accessed should be similar to expected cost of join ", expectedCost, getAccessCount() - na, 2.0);
	        }
        }
    }
	
	private int getAccessCount() {
		return dbms.getBufferManager().getNumPageAccesses();
	}


}