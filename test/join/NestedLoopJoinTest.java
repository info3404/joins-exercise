package join;

import access.read.AccessIterator;
import global.DatabaseManager;
import heap.HeapFile;
import heap.TupleDesc;
import join.data.TestData;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import parser.JoinArgs;
import utilities.TestingUtils;

import static utilities.TestingUtils.checkGetRecord;

import static org.junit.Assert.*;

public class NestedLoopJoinTest {

	// Comment this out for debugging
    @Rule
    public Timeout globalTimeout = new Timeout(1000);
    
    /**
	 * Depending upon the exact implementation, there may be some extra overhead
	 * in additional page accesses beyond the number calculated from the 
	 * algorithm. This factor gives some latitude on the actual cost.
	 */
	private static final double COST_TOLERANCE_FACTOR = 0.25;
    
    private HeapFile students;
    private HeapFile tutors;
    private DatabaseManager dbms;

    @Before
    public void setUp() throws Exception {
        dbms  = new DatabaseManager();
        
        // Create Test Schema for students
        TupleDesc studentSchema = new TupleDesc();
        studentSchema.addString("name").addInteger("age").addString("class").addBoolean("male");
        dbms.getCatalog().addSchema(studentSchema, "students");
        students = dbms.getHeapFile("students");
        
        // Create Test Schema for tutors
        TupleDesc tutorSchema = new TupleDesc();
        tutorSchema.addString("id").addString("tutor");
        dbms.getCatalog().addSchema(tutorSchema, "tutors");
        tutors = dbms.getHeapFile("tutors"); 
    }

    @After
    public void tearDown() throws Exception {
    	tutors = null;
    	students = null;
    	dbms.close();
    }

    /**
     * Simple example for case where each relation is contained in a single page
     */
    @Test
    public void testJoinSimple() throws Exception {
        TestingUtils.insertRows(students, TestData.STUDENT_ROWS_SMALL);
        TestingUtils.insertRows(tutors, TestData.TUTOR_ROWS_SMALL);
        // We will join the two relations based upon class==id
        // NOTE: First argument (inner relation name) used by query processor, not used here 
        JoinArgs args = new JoinArgs("tutors", "class", "id");

        try(AccessIterator results = new NestedLoopJoin(students.iterator(), tutors.iterator(), args)) {
            checkGetRecord(results, "Michael", 19, "INFO1103", true, "INFO1103", "Joshua");
            checkGetRecord(results, "Michael", 19, "INFO1103", true, "INFO1103", "Scott");
            checkGetRecord(results, "Jan", 18, "INFO1903", false, "INFO1903", "Steven");
            checkGetRecord(results, "Roger", 20, "INFO1103", true, "INFO1103", "Joshua");
            checkGetRecord(results, "Roger", 20, "INFO1103", true, "INFO1103", "Scott");
            assertFalse("Returning too many results", results.hasNext());
        }
    }

    /**
     * Repeat of testJoinSimple but commuting the relations
     */
    @Test
    public void testJoinSimpleCommuted() throws Exception {
        TestingUtils.insertRows(students, TestData.STUDENT_ROWS_SMALL);
        TestingUtils.insertRows(tutors, TestData.TUTOR_ROWS_SMALL);
        JoinArgs args = new JoinArgs("students", "id", "class");

        try(AccessIterator results = new NestedLoopJoin(tutors.iterator(), students.iterator(), args)) {
            checkGetRecord(results, "INFO1103", "Joshua", "Michael", 19, "INFO1103", true);
            checkGetRecord(results, "INFO1103", "Joshua", "Roger", 20, "INFO1103", true);
            checkGetRecord(results, "INFO1103", "Scott", "Michael", 19, "INFO1103", true);
            checkGetRecord(results, "INFO1103", "Scott", "Roger", 20, "INFO1103", true);
            checkGetRecord(results, "INFO1903", "Steven", "Jan", 18, "INFO1903", false);
            assertFalse("Returning too many results", results.hasNext());
        }
    }

    /**
     * Larger example in which both relations span multiple pages
     */
	@Test
    public void testJoinLarge() throws Exception {
        TestingUtils.insertRows(students, TestData.STUDENT_ROWS);
        TestingUtils.insertRows(tutors, TestData.TUTOR_ROWS);
        JoinArgs args = new JoinArgs("tutors", "class", "id");
        final int expectedCost = 1935;
        final int nRowsExpected = 8089;
        
        try(AccessIterator r=students.iterator(); AccessIterator s = tutors.iterator()) {	
	        final int na = getAccessCount();
	        try(AccessIterator results = new NestedLoopJoin(r, s, args)) {
	        	final int nb = getAccessCount();
	        	assertEquals("Join constructor should not do most of the work", 1, nb-na, 1.0);
				assertEquals("The tuples you are returning doesn't match what is expected", nRowsExpected, TestingUtils.count(results));
				assertEquals("Number of pages should be similar to expected cost of join ", expectedCost, getAccessCount() - nb, expectedCost*COST_TOLERANCE_FACTOR);
	        }
        }
    }

	private int getAccessCount() {
		return dbms.getBufferManager().getNumPageAccesses();
	}

}