package heap;

import java.util.Iterator;

import access.read.DataPageIterator;
import disk.DataPage;
import disk.Page;

/**
 * Represents a page full of records
 */
public class HeapPage extends DataPage {

	private TupleDesc schema;
	
    public HeapPage(Page page, TupleDesc schema) {
        this.data = page.getData();
    }

    public Iterator<Tuple> iterator() {
        return new DataPageIterator(this, schema);
    }

}
