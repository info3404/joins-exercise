package global;

import heap.TupleDesc;

import java.util.HashMap;
import java.util.Map;

/**
 * Stores the Schemas for the Database
 * Currently just stores the Schema in-memory, does not write it to disk
 */
public class Catalog {

    private HashMap<String, TupleDesc> schemas;

    public Catalog() {
        schemas = new HashMap<>();
    }

    /**
     * Stores the Schema definition in the Catalog
     * @param schema the description of the schema
     * @param name the name of the schema
     */
    public void addSchema(TupleDesc schema, String name) {
        if(!schemas.containsKey(name)) {
            schemas.put(name, schema);
        }
        else throw new IllegalStateException("Schema Already Exists");
    }

    /**
     * Returns the schema associated with the given name
     */
    public TupleDesc readSchema(String name) {
        return schemas.get(name);
    }

    /**
     * Returns the name of the given schema (if any)
     */
    public String findNameOfSchema(TupleDesc schema) {
        for(Map.Entry<String, TupleDesc> entry : schemas.entrySet()) {
            if(schema == entry.getValue()) {
                return entry.getKey();
            }
        }
        return "_NO_SCHEMA_FOUND_";
    }

}
