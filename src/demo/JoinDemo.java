package demo;

import access.write.AccessInserter;
import buffer.BufferManager.BufferAccessException;
import execution.QueryEngine;
import global.DatabaseManager;
import heap.HeapFile;
import heap.TupleDesc;

import java.io.IOException;

/**
 * Example Execution using Database Components.
 * See the test cases for more examples
 */
public class JoinDemo {

    public static void main(String[] args)  throws IOException, BufferAccessException {
    	DatabaseManager dbms = new DatabaseManager();
    	
        // Create Test Schema for students
        TupleDesc studentSchema = new TupleDesc();
        studentSchema.addString("name").addInteger("age").addString("class").addBoolean("male");
        dbms.getCatalog().addSchema(studentSchema, "students");
 
        HeapFile students = dbms.getHeapFile("students");

        // Create Test Schema for tutors
        TupleDesc tutorSchema = new TupleDesc();
        tutorSchema.addString("id").addString("tutor");
        dbms.getCatalog().addSchema(tutorSchema, "tutors");
        HeapFile tutors = dbms.getHeapFile("tutors");

        // Insert rows
        insertRows(students, STUDENT_ROWS_SMALL);
        insertRows(tutors, TUTOR_ROWS_SMALL);

        QueryEngine queryEngine = new QueryEngine(dbms);
        queryEngine.run();
        dbms.getBufferManager().flushDirty();
    }

    public static void insertRows(HeapFile table, Object[][] rows) throws BufferAccessException {
        try(AccessInserter inserter = table.inserter()) {
            for(Object[] row : rows) {
                inserter.insert(row);
            }
        }
    }

    public static final Object[][] STUDENT_ROWS_SMALL = new Object[][] {
            new Object[]{"Michael", 19, "INFO1103", true},
            new Object[]{"Jan", 18, "INFO1903", false},
            new Object[]{"Roger", 20, "INFO1103", true},
            new Object[]{"Rachael", 21, "ELEC1601", false}
    };

    public static final Object[][] TUTOR_ROWS_SMALL = new Object[][] {
            new Object[]{"INFO1103", "Joshua"},
            new Object[]{"INFO1103", "Scott"},
            new Object[]{"COMP2129", "Maxwell"},
            new Object[]{"INFO1903", "Steven"}
    };
}
