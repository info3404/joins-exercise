package buffer.replacement;

import buffer.BufferFrame;

import java.util.List;

/**
 * Most Recently Used Replacement Policy
 */
public class MruReplacer implements Replacer {
    @Override
    public String getName() {
        return "MRU";
    }

    @Override
    public BufferFrame choose(List<BufferFrame> pool) throws BufferFrame.AllBufferFramesPinnedException {
		for(int i=0; i<pool.size(); ++i) {
			BufferFrame frame = pool.get(pool.size() - 1 - i);
    		if(!frame.isPinned())
    			return frame;
    	}
        throw new BufferFrame.AllBufferFramesPinnedException();
    }

    @Override
    public void notify(List<BufferFrame> pool, BufferFrame frame) {
        pool.remove(frame);
        pool.add(frame);
    }
    
	public void printStats() {
		System.out.println("MRU frame replacer: No state infomation to display.");
	}
	
}
