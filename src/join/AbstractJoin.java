package join;

import access.read.AccessIterator;
import heap.Tuple;
import heap.TupleDesc;
import parser.JoinArgs;

/**
 * Abstract Join class that handles most of the default behaviour
 * This can be extended to implement concrete join classes.
 */
abstract class AbstractJoin extends AccessIterator {
	
	/**
	 * The schema for tuples produced by the join operation
	 */
    protected TupleDesc schema;
    
    // These are iterators over the left and right tables to merge.
    protected AccessIterator left;
    protected AccessIterator right;
    
    // The indexes of the columns to join on in the respective tuples.
    // To get the value to join a tuple on, we can do `tuple.getColumn(index)`
    // This would get us the value at index.
    // NOTE: Here "index" means the offset of an attribute in the schema's list of
    // attributes, NOT a data structure to locate tuples.
    protected int leftColumn;
    protected int rightColumn;

    /**
     * Constructor for generic join
     * @param left the left table to join
     * @param right the right table to join
     * @param condition the details of the join. e.g. on what columns, etc.
     */
    public AbstractJoin(AccessIterator left, AccessIterator right, JoinArgs condition) {
        this.left = left;
        this.right = right;
        this.schema = TupleDesc.join(left.getSchema(), right.getSchema());
        leftColumn = left.getSchema().getIndexFromName(condition.getLeftColumn());
        rightColumn = right.getSchema().getIndexFromName(condition.getRightColumn());
    }

    /**
     * Joins a tuple together based on its join columns
     * @param left the left tuple to join
     * @param right the right tuple to join
     * @return the joined tuple
     */
    protected Tuple joinTuple(Tuple left, Tuple right) {
        Tuple tuple = new Tuple(getSchema());
        tuple.copyValues(left);
        tuple.copyValues(right);
        return tuple;
    }

    /**
     * Release resources
     */
    @Override
    public void close() {
        left.close();
        right.close();
    }

    @Override
    public TupleDesc getSchema() {
        return schema;
    }

}
