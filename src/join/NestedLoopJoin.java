package join;

import java.util.NoSuchElementException;

import access.read.AccessIterator;
import buffer.BufferManager.BufferAccessException;
import heap.Tuple;
import parser.JoinArgs;

/**
 * Implements a Nested Loop Join.
 * ==============================
 * It's algorithm is essentially:
 * for tuple r in left:
 *  for tuple s in right:
 *      if r can be joined with s:
 *          output join(r, s)
 *
 * To check if two tuples are equal, you can use:
 *  `tupleOne.getColumn(leftColumn).equals(tupleTwo.getColumn(rightColumn)) `
 *
 * Hint: since you can't (or shouldn't) join in the constructor, you will need some way of knowing what tuple you are up
 * to. You may find you will need some temporary variables to store the current tuple you are iterating over, for example
 */
public class NestedLoopJoin extends AbstractJoin {

    private Tuple next;

    public NestedLoopJoin(AccessIterator left, AccessIterator right, JoinArgs condition) {
        super(left, right, condition);
        next = null;
    }

    /**
     * The bulk of the join will be done in the hasNext method. This is because in order to know whether there is
     * another joined tuple, you will need to find one first.
     * @return true if there is another joined tuple in the file, false if there isn't
     */
    @Override
    public boolean hasNext() {
    	// @TODO by student
        if(next != null) return true;
        return false;
    }

    /**
     * This should return the next joined tuple.
     * To join a tuple, see the GenericJoin class.
     */
    @Override
    public Tuple next() {
        if(!hasNext())
            throw new NoSuchElementException();
        Tuple temp = next;
        next = null;
        return temp;
    }
    
    @Override
    public void close() {
    	super.close();
    }
    
    /**
     * Reset the position of the right iterator to the state captured by
     * initRight in the constructor. This method can be used in hasNext() and/or
     * next() because it throws no checked exceptions, but instead catches
     * checked exceptions and re-throws as unchecked RuntimeException
     */
    private void resetRight() {
		try {
	    	right.reset();			
	    } catch (BufferAccessException e) {
			throw new RuntimeException(e);
		}
	}
    

	@Override
	public void mark() throws BufferAccessException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void reset() throws BufferAccessException {
		throw new UnsupportedOperationException();
	}
    
}
