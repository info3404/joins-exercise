package join;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import access.read.AccessIterator;
import buffer.BufferManager.BufferAccessException;
import disk.DataPage;
import heap.Tuple;
import parser.JoinArgs;

/**
 * Implements a Blocked Nested Loop Join.
 * ==============================
 * It's algorithm is essentially:
 * for block R in left:
 *   for block S in right:
 *     for tuple r in R:
 *       for tuple s in S:
 *         if r can be joined with s:
 *           output join(r, s)
 *
 * To check if two tuples are equal, you can use:
 *  `tupleOne.getColumn(leftColumn).equals(tupleTwo.getColumn(rightColumn)) `
 *
 * There have been some changes to the AccessIterator. You can now:
 *    - Reset the iterator back to the start (you may find this useful for looping)
 *    - Save the current position of the iterator, and revert back to it if you need to (only relevant to hard)
 *
 * Hint: since you can't (or shouldn't) join in the constructor, you will need some way of knowing what tuple you are up
 * to. You may find you will need some temporary variables to store the current tuple you are iterating over, for example
 */
public class BlockNestedLoopJoin extends AbstractJoin {

	private static final int DEFAULT_BLOCK_SIZE = 1;
	
    private Tuple next;
    private int framesPerBlock;

    /**
     * Main constructor
     * Feel free to expand this.
     * @param left Iterator to left-hand relation
     * @param right Iterator to right-hand relation
     * @param condition Join condition
     * @param blockSize Number of pages stored in a block for either relation
     * @throws BufferAccessException
     */
    public BlockNestedLoopJoin(AccessIterator left, AccessIterator right, JoinArgs condition, int blockSize) throws BufferAccessException {
        super(left, right, condition);
        next = null;
        framesPerBlock = blockSize;
    	// @TODO by student
    }
    
    /**
     * Variation on above constructor, using the default block size
     * You don't need to touch this one
     */
    public BlockNestedLoopJoin(AccessIterator left, AccessIterator right, JoinArgs condition) throws BufferAccessException {
        this(left, right, condition, DEFAULT_BLOCK_SIZE);
    }
    
    /**
     * The bulk of the join will be done in the hasNext method. This is because in order to know whether there is
     * another joined tuple, you will need to find one first.
     *
     * The method in this class, getNextBlock(..) should help you generate B sized blocks of tuples from your iterators
     * @return true if there is another joined tuple in the file, false if there isn't
     */
    @Override
    public boolean hasNext() {
    	// @TODO by student
        if(next != null) return true;
        return false;
    }

    /**
     * Returns a list of tuples. Or, the next block of tuples to iterate over.
     * This can be used for the Block Nested Loop algorithm to get the next block of rows to loop over.
     * @param rows the iterator to load the next B pages from
     * @return a list of tuples from B pages, or an empty list
     */
    public List<Tuple> getNextBlock(AccessIterator rows) {
        int tuplesOnPage = DataPage.getMaxRecordsOnPage(rows.getSchema());
        int tuplesPerBlock =  tuplesOnPage * (framesPerBlock);
        ArrayList<Tuple> block = new ArrayList<>(tuplesPerBlock);
        while(rows.hasNext() && block.size() < tuplesPerBlock) {
        	Tuple row = rows.next();
        	block.add(row);
        }
        return block;
    }
    
    /**
     * This should return the next joined tuple.
     * To join a tuple, see the GenericJoin class.
     */
    @Override
    public Tuple next() {
        if(!hasNext())
            throw new NoSuchElementException();
        Tuple temp = next;
        next = null;
        return temp;
    }
    
	@Override
	public void close() {
    	super.close();
    }

	@Override
	public void mark() throws BufferAccessException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void reset() throws BufferAccessException {
		throw new UnsupportedOperationException();
	}

}
