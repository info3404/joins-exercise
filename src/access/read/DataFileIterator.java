package access.read;

import java.util.NoSuchElementException;

import buffer.BufferManager;
import buffer.BufferManager.BufferAccessException;
import disk.DataPage;
import disk.PageId;
import heap.Tuple;
import heap.TupleDesc;

/**
 * Iterator over a DataFile (collection of data pages)
 */
public abstract class DataFileIterator extends AccessIterator {

    protected PageId dataPageId;
    private DataPage dataPage;
    // An iterator over the current data page in the file
    private DataPageIterator dataPageIterator;
	protected BufferManager bufferManager;
	protected TupleDesc schema;
	private PageId markedDataPageId;
	private int markedSlot;

    /**
     * Takes the PageId of the first page in the file and opens (and pins) it
     * @param dataPageId page id of the first page
     * @param schema 
     * @throws BufferAccessException 
     */
    public DataFileIterator(PageId dataPageId, BufferManager bm, TupleDesc schema) throws BufferAccessException {
    	this.schema = schema;
    	this.bufferManager = bm;
        this.dataPageId = dataPageId;
        this.dataPage = getDataPage(dataPageId);
        this.dataPageIterator = new DataPageIterator(dataPage, schema);
        mark();
    }

    /**
     * Returns a pinned data page (used to support schema/indexes)
     * @param pageId page of the page to load
     * @return pinned data page corresponding to pageId
     * @throws BufferAccessException 
     */
    protected abstract DataPage getDataPage(PageId pageId) throws BufferAccessException;

    /**
     * Close the iterator (and unpin any pages pinned by it)
     */
    @Override
    public void close() {
        dataPageIterator = null;
        bufferManager.unpin(dataPageId, false);
    }

    /**
     * Checks where there is another record in the file
     * Requires checking not only current page, but the following page
     * in case there are also records on there
     * @return true if there is another record, false if not
     */
    @Override
    public boolean hasNext() {
        if(dataPageIterator.hasNext()) {
            return true;
        }
        while(dataPage.getNextPageId().isValid()) {
            PageId next = dataPage.getNextPageId();
            bufferManager.unpin(dataPageId, false);
            try {
				dataPage = getDataPage(next);
			} catch (BufferAccessException e) {
				// Iterator hasNext doesn't allow checked exceptions
				throw new RuntimeException(e);
			}
            dataPageId = next;
            dataPageIterator = new DataPageIterator(dataPage, schema);
            if(dataPageIterator.hasNext()) return true;
        }
        return false;
    }

    /**
     * Returns the next tuple in the file.
     * Make sure that you set the page id of the tuple to the page it was read from.
     * i.e. tuple.setPageId(dataPageId);
     */
    @Override
    public Tuple next() {
        if(hasNext()) {
            Tuple next = dataPageIterator.next();
            next.setPageId(dataPageId);
            return next;
        }
        throw new NoSuchElementException("No such tuple");
    }
	
	@Override
	public TupleDesc getSchema() {
		return schema;
	}
	

	@Override
	public void mark() throws BufferAccessException {
		markedDataPageId = new PageId(this.dataPageId.get());
		markedSlot = this.dataPageIterator.getSlotNo();
	}

	@Override
	public void reset() throws BufferAccessException {
		if(!dataPageId.equals(markedDataPageId)) {
	        this.dataPageId = markedDataPageId;
	        this.dataPage = getDataPage(dataPageId);
	        this.dataPageIterator = new DataPageIterator(dataPage, schema);
		}
        dataPageIterator.setSlot(markedSlot);
	}
	
}
