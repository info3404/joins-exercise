package access.read;

import heap.Tuple;
import heap.TupleDesc;

import java.io.Closeable;
import java.util.Iterator;

import buffer.BufferManager.BufferAccessException;

/**
 * Generic Iterator Class to be used by the Database for Access patterns
 */
public abstract class AccessIterator implements Iterator<Tuple>, Closeable {

    public abstract void close();

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    public abstract TupleDesc getSchema();
    
    /**
     * Update the marked position to the current position
     * @throws BufferAccessException 
     */
	public abstract void mark()throws BufferAccessException;

    /**
     * Return to previously marked position (by default the initially constructed state), to allow algorithms to return to an earlier state
     * @throws BufferAccessException 
     */
	public abstract void reset() throws BufferAccessException;
}
