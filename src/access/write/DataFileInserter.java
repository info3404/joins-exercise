package access.write;

import java.util.NoSuchElementException;

import buffer.BufferManager;
import buffer.BufferManager.BufferAccessException;
import disk.DataPage;
import disk.PageId;
import heap.Tuple;
import heap.TupleDesc;

/**
 * Inserter class to allow for efficient insertion into a DataFile
 * - The idea is that it works similar to an iterator, in that it keeps a reference
 * to the current page alive, however these set of classes work together to allow to insert tuples.
 */
public abstract class DataFileInserter extends AccessInserter {

    private PageId dataPageId;
    private DataPage dataPage;
    private DataPageInserter dataPageInserter;
	protected BufferManager bufferManager;
	protected TupleDesc schema;

    /**
     * Takes the PageId of the first page in the file and opens (and pins) it
     * @param dataPageId page id of the first page
     * @param schema 
     * @param bufferManager 
     * @throws BufferAccessException 
     */
    public DataFileInserter(PageId dataPageId, BufferManager bufferManager, TupleDesc schema) throws BufferAccessException {
        this.dataPageId = dataPageId;
        this.bufferManager = bufferManager;
        this.schema = schema;
        this.dataPage = getDataPage(dataPageId);
        this.dataPageInserter = new DataPageInserter(dataPage, schema);
    }

    /**
     * Returns a pinned data page (used to support schema/indexes)
     * @param pageId page of the page to load
     * @return pinned data page corresponding to pageId
     * @throws BufferAccessException 
     */
    protected abstract DataPage getDataPage(PageId pageId) throws BufferAccessException;

    @Override
    public void insert(Tuple item) throws BufferAccessException {
        if(!canInsert()) {
            throw new NoSuchElementException("No such tuple");
        }
        dataPageInserter.insert(item);
    }

    @Override
    public boolean canInsert() throws BufferAccessException {
        if(dataPageInserter.canInsert()) {
            return true;
        }
        while(dataPage.getNextPageId().isValid()) {
            PageId next = dataPage.getNextPageId();
            bufferManager.unpin(dataPageId, true);
            dataPage = getDataPage(next);
            dataPageId = next;
            dataPageInserter = new DataPageInserter(dataPage, schema);
            if(dataPageInserter.canInsert()) return true;
        }
        // Create new page we don't have any space
        // Note: slightly ugly, should probably refactor into a DataPage.createNextPage() method
        PageId newPageId = bufferManager.getNewPage();
        DataPage newPage = getDataPage(newPageId);
        newPage.initialise(dataPage.getRelationName());
        newPage.setPreviousPageId(dataPageId);
        dataPage.setNextPageId(newPageId);
        bufferManager.unpin(dataPageId, true);
        dataPageId = newPageId;
        dataPage = newPage;
        dataPageInserter = new DataPageInserter(dataPage, schema);
        return dataPageInserter.canInsert();
    }

    @Override
    public void close() {
        dataPageInserter = null;
        bufferManager.unpin(dataPageId, true);
    }

    @Override
    public TupleDesc getSchema() {
        return schema;
    }
}
