package access.write;

import buffer.BufferManager;
import buffer.BufferManager.BufferAccessException;
import disk.DataPage;
import disk.PageId;
import heap.HeapPage;
import heap.TupleDesc;

/**
 * HeapFile Implementation of the DataFileInserter
 */
public class HeapFileInserter extends DataFileInserter {

    public HeapFileInserter(PageId dataPageId, BufferManager bufferManager, TupleDesc schema) throws BufferAccessException {
        super(dataPageId, bufferManager, schema);
    }

    @Override
    protected DataPage getDataPage(PageId pageId) throws BufferAccessException {
        return new HeapPage(super.bufferManager.getPage(pageId), schema);
    }
}
