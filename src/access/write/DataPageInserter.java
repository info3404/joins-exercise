package access.write;

import disk.DataPage;
import heap.Tuple;
import heap.TupleDesc;

/**
 * Inserter class to allow for easy insertion onto a DataPage
 */
public class DataPageInserter extends AccessInserter {
    private DataPage dataPage;
	private TupleDesc schema;

    /**
     * Takes a data page and initialises the current slot as 0
     * @param dataPage the page to iterate over
     * @param schema 
     */
    public DataPageInserter(DataPage dataPage, TupleDesc schema) {
        this.dataPage = dataPage;
        this.schema = schema;
    }

    @Override
    public TupleDesc getSchema() {
        return schema;
    }

    @Override
    public void insert(Tuple item) {
        dataPage.insertRecord(item);
    }

    @Override
    public boolean canInsert() {
        return dataPage.getRecordCount() < DataPage.getMaxRecordsOnPage(schema);
    }

    @Override
    public void close() {}
}
