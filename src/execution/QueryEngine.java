package execution;

import access.read.AccessIterator;
import buffer.BufferManager.BufferAccessException;
import global.DatabaseManager;
import heap.HeapFile;
import parser.Query;
import projection.Projection;
import join.NestedLoopJoin;

import java.util.Scanner;

/**
 * This class parses and executes the query given to the program by the user.
 *
 * It loops waiting for input (run method), once a command has been entered, it creates a new query and then checks that
 * it is valid. If it is, it will execute the query.
 *
 * E.g. the query SELECT name, age FROM students JOIN tutors ON class = class;
 *
 */
public class QueryEngine {
	
    private DatabaseManager dbms;

	public QueryEngine(DatabaseManager dbms) {
		this.dbms = dbms;
	}

    /**
     * Input Loop
     * - Loops over input read in from the user, validating the correctness and calling execute on valid queries
     * - Exits once "exit" is called
     * - Also will do manually timing of query execution when TIME command is entered;
     * @throws BufferAccessException 
     */
    public void run() throws BufferAccessException {
        boolean isTiming = false;
        Scanner input = new Scanner(System.in);
        String command = "";
        System.out.println("Welcome to the query engine.\n" +
                "Please enter a query below: e.g. SELECT name, age FROM students JOIN tutors ON class = class;");
        while(true) {
            System.out.print("Enter your query: ");
            command = input.nextLine();
            if(command.equals("exit")) break;
            // Supports timing command execution
            if(command.equals("TIME")) {
                isTiming = !isTiming;
                System.out.println(isTiming ? "Timing is enabled" : "Timing is disabled");
                continue;
            }
            Query query = Query.generateQuery(command);
            if(!checkQuery(query)) continue;
            // Time query and run it
            long startTime = System.nanoTime();
            AccessIterator result = execute(query);
            long endTime = System.nanoTime();
            if(isTiming) { System.out.printf("Took %.2f seconds\n", (endTime-startTime) / 1000000000.0);}
            // Print out results and close iterator
            printResult(result);
            result.close();
        }
        input.close();
    }

    /**
     * Checks that a query is valid. If a query given here is null, then it failed to be parsed properly by
     * Query.generateQuery(...). Ensures that the table name given was a valid schema, and that all column names
     * referenced can be found in that schema
     */
    private boolean checkQuery(Query query) {
        if(query == null) {
            System.out.println("Invalid formatting of query");
            return false;
        }
        String error = query.validate(dbms);
        if(error != null) {
            System.out.println(error);
        }
        return error == null;
    }

    /**
     * Executes a query.
     * 1. Loads an iterator over the rows from the table used in the query
     * 2. Joins the tuples with those from another table based upon the join arguments
     * 3. Projects/Filters the columns
     *
     * @throws BufferAccessException 
     */
    protected AccessIterator execute(Query query) throws BufferAccessException {
        HeapFile table = dbms.getHeapFile(query.getTableName());
        // Get all rows
        AccessIterator rows = table.iterator();
        if (query.hasJoinArguments()) {
            HeapFile joinTable = dbms.getHeapFile(query.getJoinArgs().getJoinTable());
            rows = new NestedLoopJoin(rows, joinTable.iterator(), query.getJoinArgs());
        }
        rows = new Projection(rows, query.getProjectedColumns());
        return rows;
    }

    /**
     * Helper method to simply print out all the entries in an access iterator
     */
    private static void printResult(AccessIterator rows) {
        while(rows.hasNext()) {
            System.out.println(rows.next().toRow());
        }
    }

}
